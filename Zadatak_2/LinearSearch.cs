﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_2
{
    class LinearSearch : SearchStrategy
    {
        public override int Search(double[] array, double x)
        {
            int arraySize = array.Length;
            for (int i = 0; i < arraySize; i++)
            {
                if (array[i] == x)
                    return i;
            }
            return -1;
        }
    }
}
