﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_2
{
    abstract class SearchStrategy
    {
        public abstract int Search(double[] array, double x);
    }
}
