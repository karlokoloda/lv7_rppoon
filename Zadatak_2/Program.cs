﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] myArray = { 5, 6, 3, -0.2, -0.765, 7564.262, 22 };
            float searchFor = 3;

            LinearSearch linearSearch = new LinearSearch();

            NumberSequence numberSequence = new NumberSequence(myArray);
            numberSequence.SetSearchStrategy(linearSearch);

            Console.WriteLine("Searched item index : " + numberSequence.Search(searchFor));

            Console.WriteLine(numberSequence.ToString());
        }
    }
}
