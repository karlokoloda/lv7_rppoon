﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_4
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;

        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }

        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (currentLoad != this.previousCPULoad)
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }

        public float GetAvailableRAM()
        {
            float currentUsage = this.AvailableRAM;
            if (GetDifference(currentUsage, this.previousRAMAvailable) >= 0.1)
            {
                this.Notify();
            }
            this.previousCPULoad = currentUsage;
            return currentUsage;
        }

        public float GetDifference (float firstValue, float secondValue)
        {
            float result;
            if (firstValue > secondValue)
            {
                result = Math.Abs(firstValue - secondValue)/firstValue;
            }
            else
            {
                result = Math.Abs(firstValue - secondValue) / secondValue;
            }
            return result;
        }
    }
}
