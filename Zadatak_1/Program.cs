﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] myArray = { 5, 6, 3, -0.2, -0.765, 7564.262, 22 };

            SequentialSort sequentialSort = new SequentialSort();
            BubbleSort bubbleSort = new BubbleSort();

            NumberSequence numberSequence = new NumberSequence(myArray);
            NumberSequence secondNumberSequence = new NumberSequence(myArray);

            numberSequence.SetSortStrategy(sequentialSort);
            secondNumberSequence.SetSortStrategy(bubbleSort);

            numberSequence.Sort();
            secondNumberSequence.Sort();

            Console.WriteLine(numberSequence.ToString());
            Console.WriteLine(secondNumberSequence.ToString());
        }
    }
}
